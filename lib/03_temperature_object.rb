class Temperature

  def initialize(options = {})
    @options = options
    @fahrenheit = @options[:f]
    @celsius = @options[:c]
  end

  def self.from_celsius(temp)
    self.new(c: temp)

  end

  def self.from_fahrenheit(temp)
    Temperature.new(f: temp)

  end

  def in_fahrenheit
    @fahrenheit || (@celsius * 9.0 / 5 + 32)

  end

  def in_celsius
    @celsius || ((@fahrenheit - 32) * 5.0 / 9)

  end


end

class Celsius < Temperature
  def initialize(temp)
    @celsius = temp
    # Temperature.new(c: temp)
  end

end


class Fahrenheit < Temperature
  def initialize(temp)
    @fahrenheit = temp
  end

end

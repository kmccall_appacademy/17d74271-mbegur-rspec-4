class Timer
  def seconds=(number)
    @seconds = number
  end

  def seconds
    @seconds = 0
  end

  def timefy(number)
    if number < 10
      return "0#{number}"
    else
      return number.to_s
    end
  end

  def time_string
    secs = @seconds % 60
    mins = (@seconds % 3600) / 60
    hrs = @seconds / 3600
    "#{timefy(hrs)}:#{timefy(mins)}:#{timefy(secs)}"
  end
end

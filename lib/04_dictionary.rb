class Dictionary
  def initialize
    @d = {}
  end

  def entries
    @d
  end

  def add(entry)
    if entry.class == String
      @d[entry] = nil
    else
      @d[entry.keys.first] = entry.values.last


    end
  end

  def keywords
    @d.keys.sort
  end

  def include?(keyword)
    @d.keys.include?(keyword)
  end

  def find(keyword)
    hash = {}
    @d.keys.each do |k|
      hash[k] = @d[k] if k.include?(keyword)
    end
    hash

  end

  def printable
    str = ""
    @d.keys.sort.each do |k|
      if str == ""
        str << "[#{k}] \"#{@d[k]}\""
      else
        str << "\n[#{k}] \"#{@d[k]}\""
      end
    end
    str
  end



end

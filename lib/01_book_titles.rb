class Book
  # TODO: your code goes here!
  def title=(book)
    result = []
    little_words = ["and", "over", "the", "a", "an", "in", "of"]
    book.split.each_with_index do |word, idx|
      if idx == 0
        result << word.capitalize
      elsif little_words.include?(word)
        result << word
      else
        result << word.capitalize
      end
    end

    @title = result.join " "

  end

  def title
    @title
  end
end
